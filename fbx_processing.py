#!/usr/bin/python
from __future__ import print_function, division, with_statement
# pyright: reportMissingModuleSource=false

import os
from os import path
import shutil
import sys
import glob
import copy
import argparse
import logging
import zipfile
import tempfile
import multiprocessing

from six.moves.urllib.request import urlopen
from six.moves.urllib.parse import urlparse


LOGGER = logging.getLogger(__name__)
TOOL_DESCRIPTION = "CLI tool for modifying your zip archives with fbx files"
TEXTURE_EXTENSIONS = ("png", "jpg")
FBX_PYTHON_PATH = os.getenv("FBX_PYTHON_SDK",
                     r"C:\Program Files\Autodesk\FBX\FBX Python SDK\2020.0.1\lib\Python27_x64")
DEBUG_ASCII_FBX = os.getenv("DEBUG_ASCII_FBX", False)
if path.isdir(FBX_PYTHON_PATH) and not FBX_PYTHON_PATH in sys.path:
    sys.path.insert(0, FBX_PYTHON_PATH)
try:
    import fbx
    import FbxCommon
except ImportError:
    logging.error("Unable to import fbx module, check PYTHONPATH variable",
                  exc_info=True)


def parse_arguments():
    parser = argparse.ArgumentParser(description=TOOL_DESCRIPTION)
    parser.add_argument("url_file", type=argparse.FileType("r"), default="urls.txt",
                        help="text file with URLs to zip files on each line")
    parser.add_argument("destination", help="path to destination directory")
    
    substitution_note = "has to contain \"{fbxfilename}\" and \"{materialname}\" in it"
    parser.add_argument("-d", "--diffuse",
                        help="texture diffuse/albedo pattern, " + substitution_note,
                        dest="diffuse_pattern",
                        default="T_{fbxfilename}_{materialname}_BC")
    parser.add_argument("-n", "--normal",
                        help="texture normal pattern, " + substitution_note,
                        dest="normal_pattern",
                        default="T_{fbxfilename}_{materialname}_N")
    parser.add_argument("-o", "--opacity",
                        help="texture opacity pattern, " + substitution_note,
                        dest="opacity_pattern",
                        default="T_{fbxfilename}_{materialname}_O")
    return parser.parse_args()


def worker(args_dict):
    tmp_dir = tempfile.mkdtemp(prefix="fbxconvert_zipped_")
    dest_path = path.join(tmp_dir, path.basename(args_dict.get("url")))
    fbx_filename = path.splitext(path.basename(dest_path))[0]
    download_archive(args_dict.get("url"), dest_path)
    unpacked_fbx_path = path.join(unpack_archive(dest_path),
                                  fbx_filename + ".fbx")
    fbx_operations(unpacked_fbx_path, args_dict.get("args"))


def download_archive(url, dest_file_path):
    """
    Args:
        url (str): valid url (can be http or file scheme)
        dest_file_path (str): 
    Returns:
        bool: result of the operation
    """
    data = urlopen(url)
    block_sz = 8192
    with open(dest_file_path, "wb") as fp:
        while True:
            buffer = data.read(block_sz)
            if not buffer:
                break
            fp.write(buffer)


def unpack_archive(archive_path):
    """
    Args:
        archive_path (str): full path to zip archive
    Returns:
        str: path to directory with extracted contents, empty if something went wrong
    """
    try:
        if not zipfile.is_zipfile(archive_path):
            raise ValueError("Not a zip file: %s" % archive_path)
        temp_dir = tempfile.mkdtemp(prefix="fbxconvert_unzipped_")
    except ValueError as exc:
        logging.error("File does not appear to be .zip file: %s" %
                      archive_path, exc_info=True)
        return ""
    except (zipfile.BadZipfile, zipfile.LargeZipFile):
        logging.error("Zip file is either corrupted or too big: %s" %
                      archive_path, exc_info=True)
        return ""
    try:
        archive = zipfile.ZipFile(archive_path)
        archive.extractall(temp_dir)
        del archive
    except Exception as exc:
        logging.error("Unable to unpack archive %s to directory %s" % (temp_dir, archive_path),
                      exc_info=True)
    try:
        logging.info(
            "Trying to remove tmp dir used to unzip files: %s" % archive_path)
        shutil.rmtree(path.dirname(archive_path))
    except Exception as exc:
        logging.warning("Unable to clean tmp dir for %s" % path.dirname(archive_path),
                        exc_info=True)
    return temp_dir


def fbx_operations(fbx_path, program_args):
    """
    Args:
        fbx_path (str): full path to valid fbx located in subfolder in temp directory
        program_args (argparse.Namespace): set of arguments we got from launching the tool
    Returns:
        bool: success status
    """
    
    fbxfilename, ext = path.splitext(path.basename(fbx_path))
    if ext != ".fbx":  # todo: add checking for binary header
        logging.error("Passed not an FBX file")
        return
    if not path.isfile(fbx_path):
        logging.error("There is no such file on disk: %s" % fbx_path)
        return
    # region Prepare fbx stuff
    manager = fbx.FbxManager.Create()
    importer = fbx.FbxImporter.Create(manager, "myImporter")
    io_settings = fbx.FbxIOSettings.Create(manager, fbx.IOSROOT)
    manager.SetIOSettings(io_settings)
    exporter = fbx.FbxExporter.Create(manager, "")
    status = importer.Initialize(fbx_path)
    if not status:
        err_msg = importer.GetStatus().GetErrorString()
        logging.error("Unable to open fbx file: %s" % fbx_path)
        logging.error("FBX SDK says: \"%s\"" % err_msg)
        return
    scene = fbx.FbxScene.Create(manager, "myScene")
    importer.Import(scene)
    importer.Destroy()
    # endregion
    
    root_node = scene.GetRootNode()
    child_nodes = [root_node.GetChild(i) for i in range(root_node.GetChildCount())]
    
    # Get materials for child nodes
    nodes_materials = {}
    for ch in child_nodes:
        nodes_materials[ch.GetName()] = [ch.GetMaterial(i) for i in range(ch.GetMaterialCount())]

    material_names = set()
    for mats in nodes_materials.values():
        for mat in mats:
            material_names.add(mat.GetName())
    
    # region Textures work
    # Look for textures
    lookup_path = path.dirname(fbx_path)
    map_types = {"diffuse": program_args.diffuse_pattern,
                 "normal": program_args.normal_pattern,
                 "opacity": program_args.opacity_pattern}
    
    material_2_tex = {}
    textures = []
    for mat_name in material_names:
        material_2_tex[mat_name] = {}
        for map_name in map_types:
            material_2_tex[mat_name][map_name] = []
            tex_filename = map_types[map_name].format(fbxfilename=fbxfilename,
                                                      materialname=mat_name)
            for ext in TEXTURE_EXTENSIONS:
                tex_paths = glob.glob(path.join(lookup_path, 
                                                   "*{}*.{}".format(tex_filename, ext)))
                material_2_tex[mat_name][map_name].extend(tex_paths)
                textures.extend(tex_paths)
            material_2_tex[mat_name][map_name].sort()
    
    
    
    if textures and nodes_materials:
        # Create texture maps and attach them to material
        for child_name in nodes_materials:
            mat_name = nodes_materials[child_name][0].GetName()
            for map_type in material_2_tex[mat_name]:
                if not material_2_tex[mat_name][map_type]:
                    continue
                fbxTexture = fbx.FbxFileTexture.Create(manager, "tex_%s" % map_type)
                tex_path = path.join(".", "textures",
                                     path.basename(material_2_tex[mat_name][map_type][0]))
                fbxTexture.SetFileName(tex_path)
                fbxTexture.SetTextureUse(fbx.FbxTexture.eStandard)
                fbxTexture.SetMappingType(fbx.FbxTexture.eUV)
                fbxTexture.SetMaterialUse(fbx.FbxFileTexture.eModelMaterial)
                fbxTexture.SetSwapUV(False)
                fbxTexture.SetTranslation(0.0, 0.0)
                fbxTexture.SetScale(1.0, 1.0)
                fbxTexture.SetRotation(0.0, 0.0)
                if map_type == "diffuse":
                    nodes_materials[child_name][0].Diffuse.ConnectSrcObject(fbxTexture)
                elif map_type == "normal":
                    nodes_materials[child_name][0].NormalMap.ConnectSrcObject(fbxTexture)
                elif map_type == "opacity":
                    nodes_materials[child_name][0].TransparentColor.ConnectSrcObject(fbxTexture)
    # endregion
    
    # Reset to origin
    for ch_node in child_nodes:
        # need to transform mesh first
        # ch_node.SetPivotState(fbx.FbxNode.eSourcePivot, fbx.FbxNode.ePivotActive)
        # ch_node.SetPivotState(fbx.FbxNode.eDestinationPivot, fbx.FbxNode.ePivotActive)
        # local_translation = [i for i in ch_node.LclTranslation.Get()]
        # print(ch_node.GeometricTranslation.Set(fbx.FbxNode.eDestinationPivot, fbx.FbxDouble3(*local_translation)))
        # ch_node.LclTranslation.Set(fbx.FbxDouble3(0, 0, 0))
        # print([i for i in ch_node.GeometricTranslation.Get()])
        # ch_node.LclRotation.Set(fbx.FbxDouble3(0, 0, 0))
        # ch_node.LclScaling.Set(fbx.FbxDouble3(1, 1, 1))
        pass

    
    # region Export scene
    manager.GetIOSettings().SetBoolProp(fbx.EXP_FBX_MATERIAL, True)
    manager.GetIOSettings().SetBoolProp(fbx.EXP_FBX_TEXTURE, True)
    manager.GetIOSettings().SetBoolProp(fbx.EXP_FBX_EMBEDDED, False)
    
    dest_dir = path.join(program_args.destination, fbxfilename, "textures")
    if not os.path.isdir(dest_dir):
        try:
            os.makedirs(dest_dir)
        except Exception:
            logging.error("Unable to create texture output folder, skipping it")
            return
    for tex in textures:
        try:
            shutil.copy(tex, dest_dir)
        except Exception:
            logging.error("Unable to copy texture %s to destination", exc_info=True)
            return
    
    file_format = manager.GetIOPluginRegistry().GetNativeWriterFormat()
    format_count = manager.GetIOPluginRegistry().GetWriterFormatCount()
    # Find ascii output format for FBX file if needed
    if DEBUG_ASCII_FBX:
        for format_index in range(format_count):
            if manager.GetIOPluginRegistry().WriterIsFBX(format_index):
                desc = manager.GetIOPluginRegistry().GetWriterFormatDescription(format_index)
                if "ascii" in desc:
                    file_format = format_index
                    break
    
    fbx_new_filename = path.realpath(path.join(program_args.destination,
                                               fbxfilename,
                                               "%s.fbx" % fbxfilename))
    if not path.isdir(path.dirname(fbx_new_filename)):
        try:
            os.makedirs(path.dirname(fbx_new_filename))
        except Exception:
            logging.error("Could not create destination directory", exc_info=True)
    result = exporter.Initialize(fbx_new_filename, file_format, manager.GetIOSettings())
    if not result:
        print(exporter.GetStatus().GetErrorString())
    exporter.Export(scene)
    # FbxCommon.SaveScene(manager, scene, fbx_new_filename)
    exporter.Destroy()
    return result
    # endregion
    


def filter_valid_urls(urls):
    """
    Args:
        urls (list): contains strings with urls
    Returns:
        set: only those strings that appeared to be valid urls
    """
    valid_urls = set()
    if urls:
        for u in urls:
            result = urlparse(u)
            if not(result.scheme == "file"
                   or result.scheme in ("http", "https") and result.netloc):
                logging.warning(
                    "Found malformed URL in the provided URL list: %s" % u)
            else:
                valid_urls.add(u)
    return valid_urls


def cleanup_tempdir():
    cleanup_files = glob.glob(path.join(
        tempfile.gettempdir(), "fbxconvert_*"))
    for fp in cleanup_files:
        try:
            shutil.rmtree(fp)
        except Exception:
            logging.warning("Unable to clean %s from temp dir" %
                            path.basename(fp))


def read_urls(url_file):
    """
    Args:
        url_file (file): full path to txt file containing urls on each line
    Returns:
        list: list of URLs if all good, None if not
    """
    try:
        with url_file as fp:
            urls = [i.strip() for i in fp.readlines()]
        url_file.close()
        return urls
    except Exception:
        logging.error("Unable to access url file", exc_info=True)
        return


def check_destination_directory(dir_path):
    """
    Checks destination path, if there is no directory, it will try to create one
    Args:
        dir_path (str): path to destination directory
    Returns:
        bool: success status
    """
    if not path.isdir(dir_path):
        try:
            os.makedirs(dir_path, 0o777)
        except OSError:
            logging.error("Unable to create specified destination directory")
            return False
    elif path.isfile(dir_path):
        logging.error(
            "Wrong destination specified, need a directory path, not a file")
        return False
    return True


def main():
    args = parse_arguments()
    urls = read_urls(args.url_file)
    if not urls: return
    valid_urls = filter_valid_urls(urls)
    dest_dir = path.realpath(args.destination)
    if not check_destination_directory(dest_dir):
        logging.error("Abort, destination path is not valid")
        return

    # Start processing archives
    argument_list = [{"url": url,
                      "args": copy.deepcopy(args)} for url in valid_urls]
    process_pool = multiprocessing.Pool(max(1, multiprocessing.cpu_count()-1))
    process_pool.map(worker, argument_list)

    cleanup_tempdir()

if __name__ == "__main__":
    main()