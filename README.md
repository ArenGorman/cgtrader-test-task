# Description
This is a CLI tool to process FBX files using FBX SDK. Unfortunately, I was not able to finish transformations part, but everything else is working.

You can configure texture patterns by using the according flags:
- `-d DIFFUSE_PATTERN`
- `-n NORMAL_PATTERN`
- `-o OPACITY_PATTERN`

Logging currently writes to STDOUT.

# Requirements
- Python
- FBX Python SDK built for the same version of Python you use

# Set up
It's simple, you just need to initialize FBX_PYTHON_SDK environment variable with valid path to FBX Python SDK installation folder, for example:
## Windows powershell
```powershell
$env:FBX_PYTHON_SDK="C:\Program Files\Autodesk\FBX\FBX Python SDK\2020.0.1\lib\Python27_x64"
```


# Usage

```
python fbx_processing.py -h
usage: fbx_processing.py [-h] [-d DIFFUSE_PATTERN] [-n NORMAL_PATTERN]
                         [-o OPACITY_PATTERN]
                         url_file destination

CLI tool for modifying your zip archives with fbx files

positional arguments:
  url_file              text file with URLs to zip files on each line
  destination           path to destination directory

optional arguments:
  -h, --help            show this help message and exit
  -d DIFFUSE_PATTERN, --diffuse DIFFUSE_PATTERN
                        texture diffuse/albedo pattern, has to contain
                        "{fbxfilename}" and "{materialname}" in it
  -n NORMAL_PATTERN, --normal NORMAL_PATTERN
                        texture normal pattern, has to contain "{fbxfilename}"
                        and "{materialname}" in it
  -o OPACITY_PATTERN, --opacity OPACITY_PATTERN
                        texture opacity pattern, has to contain
                        "{fbxfilename}" and "{materialname}" in it
```