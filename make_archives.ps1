# Loops over all subdirectories in the current directory and makes flat zip archives of their contents
foreach ($d in get-childitem -Attributes Directory) {
    $compress = @{
        Path             = "$d\*"
        CompressionLevel = "Fastest"
        DestinationPath  = "$d"
    }
    Compress-Archive @compress
}
